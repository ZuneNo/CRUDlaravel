
@extends('layouts.default')


@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Create New Item</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ route('itemCRUD.index') }}"> Back</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif



<form  method="POST" action="{{ url('/item/itemupdate') }}"  >
{!! csrf_field() !!}


    <div class="row">
      

      @foreach($editData as $edit)


        <input type="hidden" name="itemId"   value="{{ $edit->itemId  }}" id="title" placeholder="Title" style="height: 50px;" class="form-control">


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Title:</strong>

             
                <input type="textarea" name="title"   value="{{ old('title',$edit->title)}}" id="title" placeholder="Title" style="height: 50px;" class="form-control">

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description:</strong>

               
                <input type="textarea" name="description"  value="{{ old('title',$edit->description)}}" id="description" placeholder="Description" style="height: 50px;" class="form-control">

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary">Submit</button>

        </div>

        @endforeach


    </div>

</form>

@endsection