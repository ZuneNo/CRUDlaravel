@extends('layouts.default')


@section('content') 
<META HTTP-EQUIV=”Content-Type” charset="utf-8" />
 <div class="pull-right" style="float: left;">

                <a class="btn btn-success" style="text-decoration: none;" 

                href = "{{url('Customer/CustomerCreate')}}" > Create New Customer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>

            </div>



<form action="{{url('Customer/CustomerSearch')}}" method="POST">
{!! csrf_field() !!}
  Search Google:
  <input type="search" name="SearchData">
  <input type="submit">
</form>




<div class="table-responsive">

<table class="table table-bordered" border="1">

<tr>
	<th>ID</th>
	<th>Name</th>
	<th>Age</th>
	<th>Gender</th>
	<th>PhoneNo</th>
	<th colspan="2">Action</th>	
</tr>

@foreach($CustomerData as $Customer)
	<tr>
	<td>{{ $Customer->id }}</td>
	<td>{{ $Customer->Name }}</td>
	<td>{{ $Customer->Age }}</td>
	<td>{{ $Customer->Gender }}</td>
	<td>{{ $Customer->PhoneNo }}</td>
	<td><a href="{{ url('Customer/CustomerEdit/'.$Customer->id.'')}}">Edit</a></td>	
	<td><a href="{{ url('Customer/CustomerDelete/'.$Customer->id.'')}}">Delete</a></td>
	</tr>
@endforeach
</table>
</div>

@endsection

