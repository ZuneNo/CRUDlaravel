
@extends('layouts.default')


@section('content')

<META HTTP-EQUIV=”Content-Type” charset="utf-8" />
    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Create New Customer</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('Customer/CustomerList') }}"> Back</a>

            </div>

        </div>

    </div>


    



<form  method="POST" action="{{ url('/Customer/CustomerEdit') }}"  >
{!! csrf_field() !!}


    <div class="row">
      

      @foreach($editCustomer as $edit)


        <input type="hidden" name="id"   value="{{ $edit->id  }}" id="id" placeholder="id" style="height: 50px;" class="form-control">


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name:</strong>

             
                <input type="textarea" name="Name"   value="{{ old('Name',$edit->Name)}}" id="Name" placeholder="Name" style="height: 50px;" class="form-control">

            </div>

        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Age:</strong>

               
                <input type="textarea" name="Age"  value="{{ old('Age',$edit->Age)}}" id="Age" placeholder="Age" style="height: 50px;" class="form-control">

            </div>

        </div>


          <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Gender:</strong>

               
                <input type="textarea" name="Gender"  value="{{ old('Gender',$edit->Gender)}}" id="Gender" placeholder="Gender" style="height: 50px;" class="form-control">

            </div>

        </div>

          <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>PhoneNo:</strong>

               
                <input type="textarea" name="PhoneNo"  value="{{ old('PhoneNo',$edit->PhoneNo)}}" id="PhoneNo" placeholder="Description" style="height: 50px;" class="form-control">

            </div>

        </div>

         


        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary">Submit</button>

        </div>

        @endforeach


    </div>

</form>

@endsection