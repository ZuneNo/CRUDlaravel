<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ForFileUpload;
use View;

class ForFileUploadController extends Controller
{
    public function FileUpload()
    {

    	 
    	return view('ForFileUpload.FileUpload');
    }

    public function FileUploadSave(Request $request)
	{	
        $input = $request->all();
        $destinationPath = 'uploads';
        if($file = $request->file('fileToUpload')){
        $name = $file->getClientOriginalName();
        $file->move('fileUpload', $name);
        // $input['profile_pic'] = $destinationPath.$file->getClientOriginalName();
        // for name path
    }
    
 
         ForFileUpload::UploadFileSave([
                'ImagePath' => $name
            ]);


        
        return redirect('/ForFileUpload/FileUpload');
    }
	


    //PDF file is stored under project/public/download/info.pdf
  public function download($file_name) {
    $file_path = public_path('fileUpload/'.$file_name);
    return response()->download($file_path);
  }


}

