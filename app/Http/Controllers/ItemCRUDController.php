<?php
namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Item;
use View;
use Validator;

class ItemCRUDController extends Controller

{


    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)

    {

        $items = Item::orderBy('id','DESC')->paginate(5);

        return view('ItemCRUD.index',compact('items'))

            ->with('i', ($request->input('page', 1) - 1) * 5);

    }


    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        return view('ItemCRUD.create');

    }


    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {   
        

        $this->validate($request, [

            'title' => 'required',

            'description' => 'required',

        ]);


        Item::create($request->all());

   

    }


    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        $item = Item::find($id);

        return view('ItemCRUD.show',compact('item'));

    }


    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $item = Item::find($id);

        return view('ItemCRUD.edit',compact('item'));

    }


    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $this->validate($request, [

            'title' => 'required',

            'description' => 'required',

        ]);


        Item::find($id)->update($request->all());

        return redirect()->route('itemCRUD.index')

                        ->with('success','Item updated successfully');

    }


    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        Item::find($id)->delete();

        return redirect()->route('itemCRUD.index')

                        ->with('success','Item deleted successfully');

    }
    // public function viewItem(){
    //      return view('item.viewitem');
    // }
    // public function viewItem(){
    //     return redirect('create/item');

    // }
    public function ItemRegister(){
        return view('itemCRUD.create');

    }
    public function ItemCreate(request $request){

              $input=\Request::all();
              

   $validator=Validator::make($input,array("title"=>"required|email","description"=>"required"));

        if($validator->fails() )
        {
            return redirect('item/itemreg/')->withInput()->withErrors($validator);
         }
         else{
               $data=array();
              foreach ($input as $key => $value) {
                   if($key!='_token'){
                     $data[$key]=$value;
                   }
              }

              Item::insertItem($data);

              return redirect('item/itemlist');

         }
              

    }
    public function ItemList(){
       $data['ItemData']= Item::getItemList();

       return view('item.itemlist',$data);
    }
    public function ItemDelete($itemId){
          
          Item::ItemDelete($itemId);

          return redirect('item/itemlist');
    }
    public function itemEdit($itemId){

      $data['editData'] = Item::getEditData($itemId);

      return view('item.itemedit',$data);


    }
    public function itemUpdate(request $request){

          $input= \Request::all();

          $dataUpdate=array();
              
              foreach ($input as $key => $value) {
                   if($key!='_token'){
                     $dataUpdate[$key]=$value;
                   }
              }
              

             Item::ItemUpdate($dataUpdate);

             return redirect('item/itemlist');


    }

}