<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use View;
use Validator;


class CustomerController extends Controller
{
    public function CustomerList()
    {
    	$data['CustomerData'] = Customer::getCustomerList();
    	return View('Customer.CustomerList',$data);

    }

    public function CustomerCreate()
    {
    	return View('Customer.CustomerCreate');
    }

    public function CustomerSave(request $request)
    {

    	$input =\Request::all();

    	 $data=array();
              foreach ($input as $key => $value) {
                   if($key!='_token'){
                     $data[$key]=$value;
                   }
               }
    		Customer::InsertCustomer($data);
    		return redirect('Customer/CustomerList');

    }

    public function CustomerDelete($id)
    {

    	Customer::DeleteCustomer($id);

    	return redirect('Customer/CustomerList');
    }

    public function CustomerEdit($id)
    {

    	$data['editCustomer'] = Customer::getEditCustomer($id);

    	return view('Customer.CustomerEdit',$data);

    }

    public function CustomerUpdate(request $request)
    {

    	$input =\Request::all();

    	$CustomerUpdate = array();

    	foreach ($input as $key => $value)
    	 {
    		
    			if ($key!='_token') 
    			{
    				$CustomerUpdate[$key] = $value;
    			}
    	}
    	Customer::UpdateCustomer($CustomerUpdate);

    	return redirect('Customer/CustomerList');
    	}


        public function CustomerSearch(request $request)
        {
            $search = $request->SearchData;

           $data['CustomerData'] = Customer::Search($search);

           return View('Customer.CustomerList',$data);
        }


    }

