<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Customer extends Model{


		protected $table = 'Customer';
		protected $primaryKey = 'id';
		public $timestamps = false;


		public static function getCustomerList()
		{
			$table = with(new static)->table;
			$result = DB::table($table)
			->select('*')
			->get();
			return $result;
		}

		public static function InsertCustomer($data){


			$table = with(new static)->table;

			$result = DB::table($table)
					->insert($data);
					return $result;
		}

		public static function DeleteCustomer($id)
		{
			$table = with(new static)->table;
			$result = DB::table($table)
			         ->where('id','=',$id)
			         ->delete();
		}

		public static function getEditCustomer($id){

			$table=with(new static)->table;
			$result=DB::table($table)
					->where('id','=',$id)
					->select()
					->get();

			return $result;

		}


		public static function UpdateCustomer($dataUpdate)
		{
				$id = $dataUpdate['id'];
				$data['Name'] = $dataUpdate['Name'];
				$data['Age'] = $dataUpdate['Age'];
				$data['Gender'] = $dataUpdate['Gender'];
				$data['PhoneNo'] = $dataUpdate['PhoneNo'];

				$table=with(new static)->table;
     	   		$result=DB::table($table)
     	   		 ->where('id','=',$id)
     	          ->update($data); 
		}


		public static function search($searchData)
		{
			  $search = '%'.$searchData.'%';

			 $table=with(new static)->table;
			 $SearchList = DB::table($table)
                     ->where('id', 'LIKE', $search)
                     ->orwhere('Name', 'LIKE', $search)
                     ->orwhere('Age', 'LIKE', $search)
                     ->orwhere('Gender', 'LIKE', $search)
                     ->orwhere('PhoneNo', 'LIKE', $search)
                     ->get();

            return $SearchList;
		}



}




