<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Item extends Model{
 
      protected $table = 'items';
     protected $primaryKey  = 'itemId'; 
     public $timestamps = false;

     
     public static function insertItem($data){


              $table=with(new static)->table;

               $result=DB::table($table)
                    ->insert($data);
                  return $result;
       
     }
     public static function getItemList(){
     	  $table=with(new static)->table;

     	    $result=DB::table($table)
     	           ->select('*')
     	           ->get();

     	           return $result;
     }
     public static function ItemDelete($itemId){

     	  $table=with(new static)->table;
     	   $rsult=DB::table($table)
     	          ->where('itemId','=',$itemId)
     	          ->delete();


     }
     public static function getEditData($itemId){


     	  $table=with(new static)->table;
     	   $result=DB::table($table)
     	          ->where('itemId','=',$itemId)
     	          ->select()
     	          ->get();

     	        return $result;


     }
      public static function ItemUpdate($dataUpdate){
        
        $itemId=$dataUpdate['itemId'];
        $data['title']=$dataUpdate['title'];
        $data['description']=$dataUpdate['description'];

     	  $table=with(new static)->table;
     	   $result=DB::table($table)
     	          ->where('itemId','=',$itemId)
     	          ->update($data); 

     }

    

  }
  ?>