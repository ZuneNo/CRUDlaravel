<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('/itemCRUD','ItemCRUDController');
Route::post('/itemCRUD/store', 'ItemCRUDController@store');
Route::get('/item/itemreg','ItemCRUDController@ItemRegister');
Route::post('/item/itemreg','ItemCRUDController@ItemCreate');
Route::get('/item/itemlist','ItemCRUDController@ItemList');
Route::get('/item/itemdelete/{id}','ItemCRUDController@ItemDelete');
Route::get('/item/itemedit/{id}','ItemCRUDController@ItemEdit');
Route::post('/item/itemupdate','ItemCRUDController@ItemUpdate');

Route::get('/Customer/CustomerList','CustomerController@CustomerList');

Route::get('/Customer/CustomerCreate','CustomerController@CustomerCreate');

Route::post('/Customer/CustomerCreate','CustomerController@CustomerSave');

Route::get('/Customer/CustomerDelete/{id}','CustomerController@CustomerDelete');

Route::get('/Customer/CustomerEdit/{id}','CustomerController@CustomerEdit');
Route::post('/Customer/CustomerEdit','CustomerController@CustomerUpdate');

Route::get('/ForFileUpload/FileUpload','ForFileUploadController@FileUpload');
Route::post('/ForFileUpload/FileUpload','ForFileUploadController@FileUploadSave');

Route::get('/ForFileUpload/download/{file}', 'ForFileUploadController@download');

Route::post('/Customer/CustomerSearch','CustomerController@CustomerSearch');